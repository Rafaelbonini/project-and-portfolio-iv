//
//  ConversationTableViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit


private let CellIdentifier = "Cell"
private let SegueIdentifier = "ToQuestions"

class ConversationTableViewController: UITableViewController {
    
    //variables block for questions
    var SportsQuestions = ""
    var AnimalsQuestions = ""
    var BooksQuestions = ""
    var MusicQuestions = ""
    var FashionQuestions = ""
    var ArtQuestions = ""
    var FoodQuestions = ""
    var GamesQuestions = ""
    var IdolWorshipQuestions = ""
    var LanguagesQuestions = ""
    var NewsQuestions = ""
    var NaturalDisastersQuestions = ""
    var SuperHeroesQuestions = ""
    var TechnologyQuestions = ""
    var TravelQuestions = ""
    var TheSupernaturalQuestions = ""
    var MoviesQuestions = ""
    
    var conversationTopics : [Subjects] = []
    var conversationTopicsz : [String : String] = [:]
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questions()
        addConversations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return conversationTopics.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! ConversationTopicTableViewCell
        
        //set the topic of conversation to the text label in the cell
        cell.textForTopic.text = conversationTopics[indexPath.row].subject
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        //when a cell is tapped, deselect the cell and perform segue to questions VC
        
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: SegueIdentifier, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let questionsView = segue.destination as! QuestionsViewController
        
        //send apropriate information from the selected cell
        questionsView.getQuestions = conversationTopics[selectedIndex].questions
        questionsView.getTittle = conversationTopics[selectedIndex].subject
    }
    
    func addConversations(){
        conversationTopics.append(Subjects.init(subject: "Sports", questions: SportsQuestions))
        conversationTopics.append(Subjects.init(subject: "Animals", questions: AnimalsQuestions))
        conversationTopics.append(Subjects.init(subject: "Books", questions: BooksQuestions))
        conversationTopics.append(Subjects.init(subject: "Music", questions: MusicQuestions))
        conversationTopics.append(Subjects.init(subject: "Fashion", questions: FashionQuestions))
        conversationTopics.append(Subjects.init(subject: "Art", questions: ArtQuestions))
        conversationTopics.append(Subjects.init(subject: "Food", questions: FoodQuestions))
        conversationTopics.append(Subjects.init(subject: "Games", questions: GamesQuestions))
        conversationTopics.append(Subjects.init(subject: "Idol Worship", questions: IdolWorshipQuestions))
        conversationTopics.append(Subjects.init(subject: "Languages", questions: LanguagesQuestions))
        conversationTopics.append(Subjects.init(subject: "News", questions: NewsQuestions))
        conversationTopics.append(Subjects.init(subject: "Natural Disasters", questions: NaturalDisastersQuestions))
        conversationTopics.append(Subjects.init(subject: "Super Heroes", questions: SuperHeroesQuestions))
        conversationTopics.append(Subjects.init(subject: "Technology", questions: TechnologyQuestions))
        conversationTopics.append(Subjects.init(subject: "Travel", questions: TravelQuestions))
        conversationTopics.append(Subjects.init(subject: "The Supernatural", questions: TheSupernaturalQuestions))
        conversationTopics.append(Subjects.init(subject: "Movies", questions: MoviesQuestions))
    }
    func questions(){
        
        SportsQuestions = "What are some sports you like watching? Why? \n\n What are some sports you dislike watching? Why? \n\nDo you play any sports? If so, which ones?\n\nWould you like to learn how to play a sport or do an activity? What would you like to learn?\n\nWhy are sports so popular?"
        AnimalsQuestions = "What animal best represents you? Why?\n\nWhat creature scares you? Why?\n\nExcept for food, do humans need other animals? Why or why not?\n\nWhat is the most effective way to save endangered species?\n\nWhat are some examples of useful traits that help animals survive? (i.e. a giraffe’s long neck)"
        BooksQuestions = "Do you read many books?\n\nHow often do you read books?\n\nDid your parents read to you when you were a child?\n\nWhat are some of the advantages of books vs. movies? How about the disadvantages of books vs. movies?\n\nWhat was the last book you read about?\n\nDo you prefer fiction or non-fiction books?\n\nDo you think people don’t read enough books these days?"
        MusicQuestions = "Who are your favorite bands or artists?\n\nHow often do you listen to music?\n\nWhen was the last time you bought a song or album?\n\nDo you usually buy albums online or CD’s from the store?\n\nHave you ever illegally downloaded music? Do you think it is okay or not okay to download music illegally?\n\nWhat kind of music do you listen to when you want to dance?"
        FashionQuestions = "Would you like to be a fashion model? What are their lives like?\n\nCould you date someone if they had a terrible sense of fashion?\n\nWhich country or city is the most fashionable in the world?\n\nIs fashion important or not important? Why or why not?\n\nDo you prefer functional or fashionable clothing?"
        ArtQuestions = "How often do you go to art museums?\n\nDo you consider yourself to be artistic?\n\nWhat do you think about modern art paintings?\n\nHow many forms of art can you name? What is your favorite form of art?\n\nIs graffiti art? Why or why not?\n\nWhat is the most famous statue in your country?\n\nWho is your favorite artist? Why do you like them so much?"
        FoodQuestions = "What is your favorite snack?\n\nWhat unhealthy food do you love?\n\nWhat food did your mother always tell you to eat and not to eat?\n\nWhat food helps with which health problem?\n\nWhat is the future of food in your country?\n\nWhat kinds of food did you eat when you were a child? Do you eat the same things now?\n\nWhat is the best food to eat when you are sick?"
        GamesQuestions = "How many genres of video games can you name?\n\nHow are some of these genres unique and different from the others?\n\nWhat are some more examples of games?\n\nChoose one or two examples of games or video games. What are their rules?\n\nWhat kinds of games are popular now?\n\nWhat were some games that were popular in the past?\n\nWhat makes a good game?"
        IdolWorshipQuestions = "Who are the most popular singers or groups in your country?\n\nWhat are some crazy examples of fan worship you have heard of?\n\nHow far is too far when talking about fan worship?\n\nAre there any groups that you are huge fans of?\n\nWhat were some of the groups that you really loved when you were younger?"
        LanguagesQuestions = "Which languages are the most difficult to learn?\n\nWhich languages are the easiest to learn?\n\nHow many languages can you speak?\n\nDoes one of your family members or friends speak a lot of languages?\n\nWhat is the angriest sounding language?\n\nWhat do you think the oldest language is?\n\nHow many languages can you say “hello” in?"
        NewsQuestions = "Where do you get your news from?\n\nHow important is it for people to follow the news?\n\nDo you think that news agencies sometimes tell lies to make a story more popular?\n\nHow much do you trust newspapers, television news, and news from the internet? Which is the most reliable source of information?\n\nWhat kind of news stories interest you the most?"
        NaturalDisastersQuestions = "How many types of natural disaster can you name? Which is the worst?\n\nWhat natural disasters are common in your country?\n\nHave you ever been through a natural disaster? Tell your group about your experience if it isn’t too traumatic.\n\nWhat is the best / worst natural disaster movie you have seen?\n\nThink of three natural disasters. What can you do to stay safe during and after those natural disasters?"
        SuperHeroesQuestions = "Who is your favorite super hero? Why?\n\nWhat super power would you like to have?\n\nIf you had super powers would you be a super hero or a super villain?\n\nDo you prefer dark super heroes like Batman or purely good super heroes like Superman? Why?\n\nDoes a person’s favorite super hero tell you anything about their personality? What does your favorite super hero say about your personality?\n\nAre there any real super heroes?\n\nWhat is your favorite super hero movie?"
        TechnologyQuestions = "Talk about how technology has changed in your lifetime.\n\nWhat do you think has been the most important new invention in the last 100 years?\n\nAre there any new gadgets that you really want to get?\n\nWhat do you think will be the next biggest technological advance?\n\nHow can countries help to create more inventors?\n\nWhat is your favorite piece of technology you own?\n\nHow will computers change in the future?\n\nDo you think that there will be more or less new innovation in the future?"
        TravelQuestions = "Where do you like to go on vacation?\n\nWhere would you like to go on vacation?\n\nTell your partner about your best travel story.\n\nWhat are some things you always take with you on a trip?\n\nDo you prefer package tours or making your own trip?\n\nWhere did you spend your last vacation? What did you do?\n\nWhat are some of the benefits of traveling alone?\n\nWhat are some of the benefits of traveling with a group?\n\nWhat is the longest journey you have ever made?"
        TheSupernaturalQuestions = "What supernatural things do you believe in (ghosts, ESP, fortune tellers, etc.)?\n\nDo you think science will ever prove any supernatural beliefs?\n\nWhat supernatural beliefs are unique to your culture?\n\nWhat are some of the scariest supernatural movies?\n\nWhat supernatural thing do you fear?"
        MoviesQuestions = "What is your favorite genre of movie?\n\nWho are some of your favorite actors?\n\nWhat kind of movie is best for a date?\n\nDo you cry during movies?\n\nWhat is the best movie you have ever seen?\n\nWhat was the scariest movie you have ever seen?\n\nHow often do you see movies?\n\nDo you usually watch movies at the theater or watch them at home?\n\nDo you buy DVDs or download movies?"
    }
}
