//
//  ConversationTopicTableViewCell.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class ConversationTopicTableViewCell: UITableViewCell {

    @IBOutlet weak var textForTopic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
