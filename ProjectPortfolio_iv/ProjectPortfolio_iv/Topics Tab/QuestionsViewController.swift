//
//  QuestionsViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/12/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController {

    var getQuestions : String?
    var getTittle : String?
    @IBOutlet weak var questionsTextView: UITextView!
    @IBOutlet weak var navTittle: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        questionsTextView.text = getQuestions
        navTittle.title = getTittle
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
