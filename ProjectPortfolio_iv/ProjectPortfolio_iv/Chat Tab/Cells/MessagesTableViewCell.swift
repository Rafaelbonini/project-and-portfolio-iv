//
//  MessagesTableViewCell.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/19/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

class MessagesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    //SET DATA TO BE DISPLAYED IN MESSAGES TABLE CELLS
    var message:Message?{
        didSet{
            
            if let id = message?.chatPartnerId(){
                
                let ref = Database.database().reference().child("Users").child(id)
                ref.observe(.value, with: { (snapshot) in
                    if let dictionary = snapshot.value as? [String : AnyObject]{
                        
                        self.userNameLabel.text = dictionary["username"] as?
                        String
                        
                        let storageRef = Storage.storage().reference()
                        let profileImageRef = storageRef.child("ProfilePictures/\(id).jpg")
                        
                        profileImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                            if error != nil {
                                // Uh-oh, an error occurred!
                                print(error!)
                            } else {
                                // Data for "images/island.jpg" is returned
                                let image = UIImage(data: data!)
                                
                                self.profilePictureImageView.image = image
                            }
                        }
                    }
                })
            }
            self.lastMessageLabel.text = message?.text
            
            //SET TIMESTAMP FOR CELL
            if let seconds = message?.timestamp?.doubleValue{
                let timestampDate = NSDate(timeIntervalSince1970: seconds)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                
                timeLabel.text = dateFormatter.string(from: timestampDate as Date)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //ROUNDED IMAGE
        profilePictureImageView?.translatesAutoresizingMaskIntoConstraints = false
        profilePictureImageView?.layer.cornerRadius = 35
        profilePictureImageView?.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
