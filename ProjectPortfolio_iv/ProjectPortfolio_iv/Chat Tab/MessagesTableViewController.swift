//
//  MessagesTableViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/19/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let reuseId = "ReuseCell"
class MessagesTableViewController: UITableViewController {
    
    
    var messages = [Message]()
    var messagesDictionary = [String : Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        messagesDictionary.removeAll()
        observeUserMessages()
    }
    
    //get all the messages sent to and from the current user and sort them
    func observeUserMessages(){
        
        guard let uid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        
        ref.observe(.childAdded, with: { (snapshot) in
            
            let messageId = snapshot.key
            let messageReference = Database.database().reference().child("messages").child(messageId)
            
            messageReference.observe(.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String : AnyObject]{
                    let message = Message(dictionary: dictionary)
                    
                    if let chatPartnerId = message.chatPartnerId(){
                        self.messagesDictionary[chatPartnerId ] = message
                        
                        self.messages = Array(self.messagesDictionary.values)
                        self.messages.sort(by: { (message1, message2) -> Bool in
                            
                            if let m1TimeValue = message1.timestamp?.intValue, let m2timevalue = message2.timestamp?.intValue{
                                
                                return m1TimeValue > m2timevalue
                            }
                            return true
                        })
                    }
                    //decrease the number of table reloads
                    self.timer?.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
                }
            }, withCancel: nil)
        }, withCancel: nil)
    }
    
    var timer :Timer?
    
    @objc func handleReloadTable(){
        self.tableView.reloadData()
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath) as! MessagesTableViewCell
        
        let message = messages[indexPath.row]
        
        cell.message = message
        cell.lastMessageLabel.text = messages[indexPath.row].text
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    //USE USER UID TO GET INFORMATION TO SEND AND DISPLAY IN CHATLOG VC
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let message = messages[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        let ref = Database.database().reference().child("Users").child(chatPartnerId)
        
        ref.observe(.value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String : AnyObject]{
                
                let partnerUsername =  dictionary["username"] as? String
                
                self.goToChatLogWithInformation(username: partnerUsername ?? "", userToId: chatPartnerId)
            }
        }, withCancel: nil)
    }
    //Go to ChatLog screen and send selected user information
    func goToChatLogWithInformation(username: String, userToId: String){
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        
        chatLogController.userName = username
        chatLogController.toId = userToId
        
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
}
