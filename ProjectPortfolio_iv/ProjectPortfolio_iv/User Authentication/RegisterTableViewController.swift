//
//  RegisterTableViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/8/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices

class RegisterTableViewController: UITableViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var informationWarning: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var languageToPracticeTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    var imagePicker = UIImagePickerController()
    
    @IBAction func changeProfilePicture(_ sender: Any) {
        
        //make the image rounded
        profileImageView.layer.cornerRadius = profileImageView.bounds.width / 2.0
        profileImageView.layer.masksToBounds = true
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //dismiss keyboard when tap the view
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //IF REQUIREMENTS MET, TRY TO CREATE USER, IF SUCESSFUL LOG IN THE USER
    @IBAction func createNewAccount(_ sender: Any) {
        
        if emailTextField.text != ""
            && (passwordTextField.text?.count)! > 6
            && (usernameTextField.text?.count)! > 6
            && fullNameTextField.text != ""
            && profileImageView != nil
            && languageToPracticeTextField.text != ""
            && ageTextField.text != ""{
            
            let username = usernameTextField.text!
            let email = emailTextField.text!
            let password = passwordTextField.text!
            let fullname = fullNameTextField.text!
            let languageToPractice = languageToPracticeTextField.text!
            let age = ageTextField.text!
            
            Auth.auth().createUser(withEmail: email, password: password, completion: { (firUser, error) in
                if error != nil{
                    //print error
                    print(error!)
                }else if let firUser = firUser{
                    
                    let newUser = User(uid: firUser.uid, username: username, profileImage: self.profileImageView.image!, email: email, fullName: fullname, age:age , laguageToPractice: languageToPractice, bio: "")
                    
                    
                    newUser.save(completion: { (error) in
                        if error != nil{
                            print(error!)
                        }else{
                            self.dismiss(animated: true, completion: nil)
                        }
                    })
                    
                    Auth.auth().signIn(withEmail: email, password: password, completion: { (firUser, error) in
                        if error != nil{
                            print(error!)
                        }else{
                        }
                    })
                }
            })
        }else{
            //if requirements were not met for creating account
            informationWarning.isHidden = false
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        //only use the media if the media is a photo
        if mediaType == (kUTTypeImage as String ){
            self.profileImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
        }
        self.dismiss(animated: true, completion: nil)
    }
}
