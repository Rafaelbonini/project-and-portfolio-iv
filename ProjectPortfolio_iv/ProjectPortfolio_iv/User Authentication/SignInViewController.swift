//
//  SignInViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/8/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
    
    @IBOutlet weak var lonInWarning: UILabel!
    @IBOutlet weak var logInEmailTextField: UITextField!
    @IBOutlet weak var logInPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //dismiss keyboard when tap the view
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        // Do any additional setup after loading the view.
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil{
                //user is logged in, go to initial viewC
                self.dismiss(animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil) 
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //try to log in with email and password user was entered
    @IBAction func LogIn(_ sender: Any) {
        //check if both email and password have characters requirements
        if (logInEmailTextField.text?.count)! > 6
            && (logInPasswordTextField.text?.count)! > 6{
            
            let logInEmail = logInEmailTextField.text!
            let logInPassword = logInPasswordTextField.text!
            
            //log in with email and password
            Auth.auth().signIn(withEmail: logInEmail, password: logInPassword, completion: { (firUser, error) in
                if let error = error{
                    print(error)
                    //error at login, display message
                    self.lonInWarning.isHidden = false
                }else{
                    //user sucessfully logged in
                    self.lonInWarning.isHidden = true
                }
            })
        }else{
            //email or password do not meet the characters requirement
            lonInWarning.isHidden = false
        }
    }

    //unwind from registerVC to this VC
    @IBAction func unwindToRoot(segue: UIStoryboardSegue){
        let _ = segue.source as! RegisterTableViewController
    }
}
