//
//  TableViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/8/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import GeoFire

private let ShowAuth = "ShowAuth"
private let CellIdentifier = "cells"
private let ToProfileSegue = "ToProfile"
private let ToSettingsSegue = "ToSettings"
class TableViewController: UITableViewController, CLLocationManagerDelegate, SendDataBackProtocol, UpdateSearchRangeProtocol {
    func sendUpratedSearchRange(searchRange: Double) {
        rangeForLocationSearch = searchRange
        backFromProfileView = nil
    }
    
    func getUserLocationAgain(startUpdatingLocation: Bool) {
        backFromProfileView = startUpdatingLocation
    }
    
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    var usersInformationStorage : [NearUsers] = []
    var selectedIndex = 0
    let locationManager = CLLocationManager()
    var backFromProfileView :Bool?
    var userUid = ""
    var usersInRange : [String] = []
    var distance : [Double] = []
    var rangeForLocationSearch: Double = 20
    var clearArrays = true
    
    @IBAction func goToSettings(_ sender: Any) {
        performSegue(withIdentifier: ToSettingsSegue, sender: self)
    }
    //avoid bug with settings button remaining faded
    override func viewWillDisappear(_ animated: Bool) {
        settingsButton.isEnabled = false
        settingsButton.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        clearArrays = true
        print("CLEAR ARRAY STATYS: \(clearArrays)")
        
        //check if user was been authenticated
        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != nil{
                
                self.userUid = (user?.uid)!
                
                self.locationManager.delegate = self
                self.locationManager.requestWhenInUseAuthorization()
                
                if self.backFromProfileView == false{
                    
                }else{
                    self.locationManager.startUpdatingLocation()
                }
                
                //the user did log in
            }else{
                self.backFromProfileView = nil
                //user is not currently logged
                self.performSegue(withIdentifier: ShowAuth, sender: nil)
            }
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return distance.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as! FindPeopleTableViewCell
        // Create a reference to the file you want to download
        
        print("USER UID \(userUid)")
        
        let storageRef = Storage.storage().reference()
        let profileImageRef = storageRef.child("ProfilePictures/\(usersInRange[indexPath.row]).jpg")
        
        //creating reference to database
        let ref = Database.database().reference()
        //getting the user information using its uid
        ref.child("Users").child(usersInRange[indexPath.row]).observeSingleEvent(of: .value, with: { (snapshot) in
            
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["username"] as? String ?? ""
            let languageToLearn = value?["TargetLanguage"] as? String ?? ""
            let fullName = value?["fullName"] as? String ?? ""
            let age = value?["age"] as? String ?? ""
            let bio = value?["bio"] as? String ?? ""
            
            cell.userNameLabel.text = username
            cell.targetLanguage.text = "Practicing: \(languageToLearn)"
            
            DispatchQueue.main.async {
                
                var usersUidStored : [String] = []
                
                for users in self.usersInformationStorage{
                    
                    usersUidStored.append(users.uid)
                }
              
                if usersUidStored.contains(self.usersInRange[indexPath.row]){
                    
                }else{
                    self.usersInformationStorage.append(NearUsers(targetlanguage: languageToLearn, age: age, fullname: fullName, username: username, uid: self.usersInRange[indexPath.row],bio: bio))
                }
            }
            // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
            profileImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if error != nil {
                    // Uh-oh, an error occurred!
                    print(error!)
                } else {
                    // Data for "images/island.jpg" is returned
                    let image = UIImage(data: data!)
                
                    DispatchQueue.main.async {
                        cell.profilePictureImageView.image = image
                    }
                    
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        var distance : Int = Int(self.distance[indexPath.row])
        
        distance = distance/1000
        
        if (distance < 2){
            cell.distancelabel.text = "less than 2 km"
        }else{
            cell.distancelabel.text = "\(distance) Km"
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        //when a cell is tapped, deselect the cell and perform segue to questions VC
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: ToProfileSegue, sender: self)
    }
    
    //MARK: - Prepare for segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == ShowAuth{
            
        }else if segue.identifier == ToProfileSegue{
            
            let profileView = segue.destination as! ProfileViewController
            profileView.SendDataBackProtocol = self
            
            profileView.getSelectedUser  = usersInformationStorage[selectedIndex]
            
        }else if segue.identifier == ToSettingsSegue{
            let settingsview = segue.destination as! SettingsTableViewController
            settingsview.valueForRangeLable = "\(rangeForLocationSearch)"
            settingsview.updateSearchRangeProtocol = self
            
        }
    }
    //MARK: - UPDATE LOCATION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations[0].coordinate.latitude)
        print(locations[0].coordinate.longitude)
        
        let geofireRef = Database.database().reference().child("users_locations")
        let geofire = GeoFire(firebaseRef: geofireRef)
        
        geofire.setLocation(CLLocation(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), forKey: userUid)
        
        locationManager.stopUpdatingLocation()
        
        //get other users within range
        
        let center = CLLocation(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        print("\n\n\n\n\n\nRANGE FOR LOCATION:  \(rangeForLocationSearch)")
        let circleQuery = geofire.query(at: center, withRadius: rangeForLocationSearch)
        
        
        //query to get users in a certain range
        _ = circleQuery.observe(.keyEntered) { (key:String!, location: CLLocation) in
            print("Key '\(key!)' entered the search area and is at location '\(location)'")
            
            //remove all items from arrays before filling them with new data and reload table
            if self.clearArrays == true{
                
                print("BEFORE\(self.usersInRange)")
                self.usersInRange.removeAll()
                self.distance.removeAll()
                print("aARHG AFTER  \(self.usersInRange)")
                self.usersInformationStorage.removeAll()
                self.clearArrays = false
                print("aARHG \(self.usersInformationStorage)")
                
            }
            
            let distanceInMeters = center.distance(from: location)
            
            print("USER UID \(self.userUid)     KEY  \(key!)")
            // not allow the current user and duplicates uid and distance into the arrays
            if(self.userUid != key!){
                
                print("RUN USER UID")
                if (self.usersInRange.count >= 1){
                    
                    if !self.usersInRange.contains(key!){
                        //saving the user Uid and de distance from the current user
                        self.usersInRange.append(key!)
                        self.distance.append(distanceInMeters)
                        
                    }
                }else if (self.usersInRange.count == 0){
                    //saving the user Uid and de distance from the current user
                    self.usersInRange.append(key!)
                    self.distance.append(distanceInMeters)
                }
                
                //LIMIT NUMBER OF TABLE RELOADS
                self.timer?.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
                
            }
        }
    }
    
    var timer :Timer?
    
    @objc func handleReloadTable(){
        self.tableView.reloadData()
    }
}
