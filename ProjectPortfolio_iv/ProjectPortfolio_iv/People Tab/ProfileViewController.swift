//
//  ProfileViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase


protocol SendDataBackProtocol {
    func getUserLocationAgain(startUpdatingLocation: Bool)
}

class ProfileViewController: UIViewController {
    
    var SendDataBackProtocol : SendDataBackProtocol?
    
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var targetLanguageLabel: UILabel!
    @IBOutlet weak var navTittle: UINavigationItem!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    var getSelectedUser : NearUsers?
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        
        //DON'T RELOAD TABLE VIEW CONTROLLER WHEN COMING BACK FROM THIS VIEW
        SendDataBackProtocol?.getUserLocationAgain(startUpdatingLocation: false)
        
        //RECEIVING DATA FORM TABLEVIEWCONTROLER
        self.navTittle.title = getSelectedUser?.username
        self.fullNameLabel.text = "\(getSelectedUser?.fullname ?? "")"
        self.ageLabel.text = "Age: \(getSelectedUser?.age ?? "")"
        self.targetLanguageLabel.text = "Language To Practice: \(getSelectedUser?.targetlanguage ?? "")"
        self.bioTextView.text = getSelectedUser?.bio
        
        let storageRef = Storage.storage().reference()
        let profileImageRef = storageRef.child("ProfilePictures/\(getSelectedUser?.uid ?? "").jpg")
        
        profileImageRef.getData(maxSize: 10 * 1024 * 1024) { data, error in
            if error != nil {
                // Uh-oh, an error occurred!
                print(error!)
            } else {
                // Data for "ProfilePictures/userUID.jpg" is returned
                let image = UIImage(data: data!)
                
                DispatchQueue.main.async {
                    self.profilePictureImageView.image = image
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MAKE IMAGE ROUNDED
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.bounds.width / 2.0
        profilePictureImageView.layer.masksToBounds = true
        
    }
    var messagesTableVC = MessagesTableViewController()
    
    //GO TO CHATLOG WITH USER INFORMATION
    @IBAction func ChatWithUser(_ sender: Any) {
        
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        
        chatLogController.toId = getSelectedUser?.uid ?? ""
        chatLogController.userName = getSelectedUser?.username
        navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
