//
//  EditProfileTableViewController.swift
//  
//
//  Created by Rafael Bonini on 3/17/18.
//

import UIKit
import Firebase

class EditProfileTableViewController: UITableViewController {
    
    var userUId = ""
    var DatabaseRef = Database.database().reference()

    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var targetLanguageTextField: UITextField!
    @IBOutlet weak var bioTextView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        userUId = Auth.auth().currentUser!.uid
        
        //get the user information to fill the page
        DatabaseRef.child("Users").child(userUId).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let username = value?["username"] as? String ?? ""
            let fullName = value?["fullName"] as? String ?? ""
            let age = value?["age"] as? String ?? ""
            let targetLanguage = value?["TargetLanguage"] as? String ?? ""
            let bioText = value?["bio"] as? String ?? ""
            
            self.usernameTextField.text = username
            self.fullNameTextField.text = fullName
            self.ageTextField.text = age
            self.targetLanguageTextField.text = targetLanguage
            self.bioTextView.text = bioText
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func Cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //if the data was changes, update the user information in the database
    @IBAction func SaveData(_ sender: Any) {
        if (usernameTextField.text?.count)! > 6{
            
            let ref = DatabaseRef.child("Users").child(userUId)
            ref.updateChildValues(toDictionary())
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func toDictionary() -> [String : Any]
    {
        return [
            "username" : usernameTextField.text ?? "",
            "fullName" : fullNameTextField.text ?? "",
            "age" : ageTextField.text ?? "",
            "TargetLanguage" : targetLanguageTextField.text ?? "",
            "bio" : bioTextView.text ?? ""
        ]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
