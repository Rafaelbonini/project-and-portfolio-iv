//
//  SettingsTableViewController.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/15/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices


protocol UpdateSearchRangeProtocol {
    func sendUpratedSearchRange(searchRange: Double)
}

class SettingsTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    //storage reference
    let storage = Storage.storage().reference()
    
    @IBOutlet weak var profilePictureImageView: UIImageView!
    var updateSearchRangeProtocol : UpdateSearchRangeProtocol?
    var valueForRangeLable = ""
    var userUId = ""
    @IBOutlet weak var rangeSliderLabel: UILabel!
    @IBOutlet weak var distanceSlider: UISlider!
    var imagePicker = UIImagePickerController()
    
    @IBAction func LogOut(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            //   self.dismiss(animated: true, completion: nil)
            
            if let navigat = navigationController{
                navigat.popToRootViewController(animated: true)
            }
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //get current user uid
        userUId = Auth.auth().currentUser!.uid
        
        //set slider and slider label with the value used in the initial view
        rangeSliderLabel.text = "\(valueForRangeLable)Km"
        distanceSlider.value = Float(valueForRangeLable)!

        //make the image rounded
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.bounds.width / 2.0
        profilePictureImageView.layer.masksToBounds = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userUId = Auth.auth().currentUser!.uid

        //GET THE USER PROFILE IMAGE TO DISPLAY
        let profileImageRef = storage.child("ProfilePictures/\(userUId).jpg")
        print("hahaha \(profileImageRef)")
        
        profileImageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if error != nil {
                // Uh-oh, an error occurred!
                print(error!)
            } else {
                // Data for "images/island.jpg" is returned
                let image = UIImage(data: data!)
                
                DispatchQueue.main.async {
                    self.profilePictureImageView.image = image
                }
            }
        }
    }
    
    @IBAction func ChangeProfilePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //KEEP SLIDER LABEL UPDATED AND SEND VALUE TO TABLEVIEW TO SET THE NEW DISTANCE RANGE
    @IBAction func UpdateLabel(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        DispatchQueue.main.async {
            self.rangeSliderLabel.text = "\(currentValue).0Km"
            self.updateSearchRangeProtocol?.sendUpratedSearchRange(searchRange: Double(currentValue))
        }
    }
    
    //DISPLAY IMAGE PICKER AND GET THE SELECTED IMAGE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        //only use the media if the media is a photo
        if mediaType == (kUTTypeImage as String){
            self.profilePictureImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            if let imageData = UIImageJPEGRepresentation((profilePictureImageView.image?.resize())!, 0.1){
                
                let storageRefere = storage.child("ProfilePictures/\(userUId).jpg")
                
                storageRefere.putData(imageData, metadata: nil){ (metadata, error) in
                    
                    if error != nil{
                        print(error!)
                        
                    }else{
                        
                        print("Done")
                    }
                    
                    guard let metadata = metadata else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    let _ = metadata.downloadURL
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//resize image
private extension UIImage{
    
    func  resize() -> UIImage {
        let height: CGFloat = 1000.0
        let ratio = self.size.width / self.size.height
        let width = height * ratio
        
        let newSize = CGSize(width: width, height: height)
        let newRectangle = CGRect(x: 0, y: 0, width: width, height: height)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: newRectangle)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
}
