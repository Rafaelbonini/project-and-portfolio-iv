//
//  FindPeopleTableViewCell.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class FindPeopleTableViewCell: UITableViewCell {

    @IBOutlet weak var distancelabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var targetLanguage: UILabel!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // ROUNDED IMAGE
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.bounds.width / 2.0
        profilePictureImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
