//
//  User.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/8/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

class User
{

    var username: String
    let uid: String
    var fullName: String
    var profileImage: UIImage?
    var email: String
    var age:String
    var laguageToPractice:String
    var bio: String
    //storage reference
    let storage = Storage.storage().reference()
    
    var sent = false
    
    init(uid: String, username: String, profileImage: UIImage, email: String, fullName: String,age: String,laguageToPractice: String, bio:String)
    {
        self.uid = uid
        self.username = username
        self.profileImage = profileImage
        self.email = email
        self.fullName = fullName
        self.age = age
        self.laguageToPractice = laguageToPractice
        self.bio = bio
    }
    
    
    // save the user information
    
    func save(completion: @escaping (Error?) -> Void)
    {
        
        // reference to the database
        let ref = Database.database().reference().child("Users").child(uid)
        
        
        // setValue of the reference
        ref.setValue(toDictionary())
        
        // 3. save the user's profile Image to the storage
        
        if let imageData = UIImageJPEGRepresentation(profileImage!.resize(), 0.1){
            
            let storageRefere = storage.child("ProfilePictures/\(uid).jpg")
            
            storageRefere.putData(imageData, metadata: nil){ (metadata, error) in
                
                if error != nil{
                    print(error!)
                    
                }else{
                    
                    print("naicela")
                }
                
                guard let metadata = metadata else {
                    // Uh-oh, an error occurred!
                    return
                }
                let _ = metadata.downloadURL
                
            }
        }
    }
    
    
    func toDictionary() -> [String : Any]
    {
        return [
            "uid" : uid,
            "username" : username,
            "fullName" : fullName,
            "email" : email,
            "age" : age,
            "TargetLanguage" : laguageToPractice,
            "bio" : ""
        ]
    }
}
//resize profile picture to reduce time to load
private extension UIImage{
    
    func  resize() -> UIImage {
        let height: CGFloat = 1000.0
        let ratio = self.size.width / self.size.height
        let width = height * ratio
        
        let newSize = CGSize(width: width, height: height)
        let newRectangle = CGRect(x: 0, y: 0, width: width, height: height)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: newRectangle)
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return resizedImage!
    }
    
}
