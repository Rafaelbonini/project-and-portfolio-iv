//
//  NearUsers.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit

class NearUsers{
    
    var targetlanguage : String
    var  age : String
    var fullname : String
    var  username : String
    var uid : String
    var bio : String

    init(targetlanguage: String, age: String,fullname: String, username: String,uid: String, bio: String) {
        self.targetlanguage = targetlanguage
        self.age = age
        self.fullname = fullname
        self.username = username
        self.uid = uid
        self.bio = bio
    }
    
}
