//
//  Message.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/19/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
class Message: NSObject {
    
    var fromId: String?
    var text: String?
    var timestamp: NSNumber?
    var toId: String?
    
    init(dictionary: [String: Any]) {
        self.fromId = dictionary["fromId"] as? String
        self.text = dictionary["text"] as? String
        self.toId = dictionary["toId"] as? String
        self.timestamp = dictionary["timestamp"] as? NSNumber
    }
    
    func chatPartnerId() -> String? {
        
        if fromId == Auth.auth().currentUser?.uid
        {
            return toId
        }else{
            return   fromId
        }
    }
}
