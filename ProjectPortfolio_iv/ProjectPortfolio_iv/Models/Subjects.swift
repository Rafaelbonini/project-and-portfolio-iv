//
//  Subjects.swift
//  ProjectPortfolio_iv
//
//  Created by Rafael Bonini on 3/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import Foundation

class Subjects{
    
    var subject: String
    var questions : String

    init(subject: String, questions: String) {
        self.subject = subject
        self.questions = questions
    }
    
}
